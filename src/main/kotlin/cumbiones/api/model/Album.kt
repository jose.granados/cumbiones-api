package cumbiones.api.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
data class Album(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long? = null,

    val name: String? = "",

    @OneToMany(mappedBy = "album")
    val tracks: List<Track>? = null,

    val releaseDate: String? = "",

    @ManyToOne
    val artist: Artist? = null
)
