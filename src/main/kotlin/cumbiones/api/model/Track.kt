package cumbiones.api.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Track (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long?,

    val name: String?,

    val lyrics: String? = "",

    @ManyToOne
    val album: Album? = null
)
