package cumbiones.api.musixmatch.client

import cumbiones.api.config.CumbionesProperties
import cumbiones.api.musixmatch.model.Response
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class MusixMatchClient(
    val restTemplate: RestTemplate,
    val props: CumbionesProperties
) {
    private fun getResponse(url: String, id: String, apikey: String) =
        restTemplate.getForObject(url, Response::class.java, id, apikey)

    fun getArtist(artistId: String): Flowable<Response?> = Flowable.fromCallable {
        getResponse(props.artistUrl, artistId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())

    fun getArtistAlbums(artistId: String): Flowable<Response> = Flowable.fromCallable {
        getResponse(props.artistAlbumsUrl, artistId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())

    fun getArtistAlbum(albumId: String): Flowable<Response?> = Flowable.fromCallable {
        getResponse(props.artistSingleAlbumUrl, albumId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())

    fun getAlbumTracks(albumId: String): Flowable<Response> = Flowable.fromCallable{
        getResponse(props.albumTracksUrl, albumId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())

    fun getTrack(trackId: String): Flowable<Response> = Flowable.fromCallable {
        getResponse(props.singleTrackUrl, trackId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())

    fun getTrackLyrics(trackId: String): Flowable<Response> = Flowable.fromCallable {
        getResponse(props.trackLyricsUrl, trackId, props.musixMatchApiKey)
    }.subscribeOn(Schedulers.io())
}
