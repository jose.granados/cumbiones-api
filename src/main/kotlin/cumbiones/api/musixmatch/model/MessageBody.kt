package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MessageBody(
    val artist: MusixMatchArtist?,

    val album: MusixMatchAlbum?,

    val track: MusixMatchTrack?,

    val lyrics: MusixMatchTrackLyrics?,

    @JsonProperty("album_list")
    val albumList: List<MusixMatchAlbumWrapper>?,

    @JsonProperty("track_list")
    val trackList: List<MusixMatchTrackWrapper>?
)
