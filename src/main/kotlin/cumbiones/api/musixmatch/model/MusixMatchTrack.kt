package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MusixMatchTrack (
    @JsonProperty("track_id")
    val trackId: String,

    @JsonProperty("track_name")
    val trackName: String,

    @JsonProperty("album_id")
    val albumId: String,

    @JsonProperty("album_name")
    val albumName: String
)
