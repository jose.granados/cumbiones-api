package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MusixMatchTrackLyrics(
    @JsonProperty("lyrics_id")
    val lyricsId: String,

    @JsonProperty("lyrics_body")
    val lyricsBody: String
)
