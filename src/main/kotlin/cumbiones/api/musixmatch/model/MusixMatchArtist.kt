package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MusixMatchArtist(
    @JsonProperty("artist_id")
    val artistId: String,

    @JsonProperty("artist_name")
    val artistName: String
)
