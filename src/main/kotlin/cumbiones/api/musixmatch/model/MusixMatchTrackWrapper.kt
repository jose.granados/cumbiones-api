package cumbiones.api.musixmatch.model

data class MusixMatchTrackWrapper (
    val track: MusixMatchTrack
)
