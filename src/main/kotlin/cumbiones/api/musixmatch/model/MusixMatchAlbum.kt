package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MusixMatchAlbum(
    @JsonProperty("album_id")
    val albumId: String,

    @JsonProperty("album_name")
    val albumName: String,

    @JsonProperty("artist_id")
    val artistId: String,

    @JsonProperty("artist_name")
    val artistName: String,

    @JsonProperty("album_release_date")
    val releaseDate: String
)
