package cumbiones.api.musixmatch.model

import com.fasterxml.jackson.annotation.JsonProperty

data class MusixMatchAlbumWrapper(
    val album: MusixMatchAlbum
)
