package cumbiones.api.musixmatch.model

data class Response(
    val message: Message
)