package cumbiones.api.musixmatch.model

data class Message(
    val body: MessageBody
)