package cumbiones.api.service

import cumbiones.api.model.Album
import cumbiones.api.musixmatch.client.MusixMatchClient
import cumbiones.api.model.Artist
import cumbiones.api.model.Track
import cumbiones.api.musixmatch.model.MusixMatchAlbum
import cumbiones.api.repository.ArtistRepository
import io.reactivex.Flowable
import io.reactivex.rxkotlin.Flowables
import org.springframework.stereotype.Service

@Service
class CumbionesService (
  val artistRepository: ArtistRepository,
    val musixMatchClient: MusixMatchClient
) {

    fun getAllArtists (): MutableIterable<Artist>? = artistRepository.findAll()

    fun getArtist(artistId: String): Flowable<Artist>? {
        return musixMatchClient.getArtist(artistId).map {
            Artist(
                id = it.message.body.artist?.artistId?.toLongOrNull(),
                name = it.message.body.artist?.artistName
            )
        }
    }

    fun getArtistAlbums(artistId: String): Flowable<List<Album>>? = musixMatchClient.getArtistAlbums(
        artistId
    ).map {
        it.message.body.albumList?.map {
            getAlbum(it.album)
        }
    }

    fun getArtistAlbum(albumId: String): Flowable<Album>? = musixMatchClient.getArtistAlbum(
        albumId
    ).map {
        getAlbum(it.message.body.album)
    }

    fun getAlbumTracks(albumId: String): Flowable<List<Track>>? = musixMatchClient.getAlbumTracks(
        albumId
    ).map {
        it.message.body.trackList?.map {
            Track(id = it.track.trackId.toLongOrNull(), name = it.track.trackName)
        }
    }

    fun getTrack(trackId: String): Flowable<Track>? =
        Flowables.zip(
            musixMatchClient.getTrack(trackId),
            musixMatchClient.getTrackLyrics(trackId)
        ) { track, lyrics -> Track(
            id = track.message.body.track?.trackId?.toLongOrNull(),
            name = track.message.body.track?.trackName,
            lyrics = lyrics.message.body.lyrics?.lyricsBody)
        }

    private fun getAlbum(album: MusixMatchAlbum?): Album {
        return Album(
            id = album?.albumId?.toLongOrNull(),
            name = album?.albumName,
            releaseDate = album?.releaseDate,
            artist = Artist( id = album?.artistId?.toLongOrNull(), name = album?.artistName)
        )
    }
}