package cumbiones.api.controller

import cumbiones.api.model.Album
import cumbiones.api.model.Artist
import cumbiones.api.model.Track
import cumbiones.api.service.CumbionesService
import io.reactivex.Flowable
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CumbionesController(
    val cumbionesService: CumbionesService
) {

    @RequestMapping("/artists")
    fun getAllArtists(): Iterable<Artist>? {
        return cumbionesService.getAllArtists()
    }

    @RequestMapping("/artists/{artistId}")
    fun getArtist(@PathVariable artistId: String): Flowable<Artist>? {
        return cumbionesService.getArtist(artistId)
    }

    @RequestMapping("/artists/{artistId}/albums")
    fun getArtistAlbums(@PathVariable artistId: String): Flowable<List<Album>>? {
        return cumbionesService.getArtistAlbums(artistId)
    }

    @RequestMapping("/artists/{artistId}/albums/{albumId}")
    fun getArtistAlbum(@PathVariable albumId: String): Flowable<Album>? {
        return cumbionesService.getArtistAlbum(albumId)
    }

    @RequestMapping("/artists/{artistId}/albums/{albumId}/tracks")
    fun getAlbumTracks(@PathVariable albumId: String): Flowable<List<Track>>? {
        return cumbionesService.getAlbumTracks(albumId)
    }

    @RequestMapping("/artists/{artistId}/albums/{albumId}/tracks/{trackId}")
    fun getTrack(@PathVariable trackId: String): Flowable<Track>? {
        return cumbionesService.getTrack(trackId)
    }
}