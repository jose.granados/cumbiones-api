package cumbiones.api.repository

import cumbiones.api.model.Artist
import org.springframework.data.repository.CrudRepository

interface ArtistRepository: CrudRepository<Artist, Long>

