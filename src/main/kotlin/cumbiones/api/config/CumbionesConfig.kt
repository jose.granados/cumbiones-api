package cumbiones.api.config

import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.util.Collections
import java.net.InetSocketAddress
import org.springframework.http.client.SimpleClientHttpRequestFactory
import java.net.Proxy

@Component
class CumbionesConfig {

    @Bean
    fun restTemplate(): RestTemplate {

        // TODO: Remove if no proxy is needed
        val requestFactory = SimpleClientHttpRequestFactory()

        val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("127.0.0.1", 3128))
        requestFactory.setProxy(proxy)
        val restTemplate = RestTemplate(requestFactory)
        // end TODO

        val messageConverters = arrayListOf<HttpMessageConverter<*>>()
        val converter = MappingJackson2HttpMessageConverter()

        converter.supportedMediaTypes = Collections.singletonList(MediaType.ALL)
        messageConverters.add(converter)

        restTemplate.messageConverters = messageConverters

        return restTemplate
    }
}