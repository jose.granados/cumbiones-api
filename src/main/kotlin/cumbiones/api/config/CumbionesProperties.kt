package cumbiones.api.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("cumbiones")
class CumbionesProperties (
    var musixMatchApiKey: String = "",
    var artistUrl: String ="",
    var artistAlbumsUrl: String = "",
    var artistSingleAlbumUrl: String = "",
    var albumTracksUrl: String = "",
    var singleTrackUrl: String = "",
    var trackLyricsUrl: String = ""
)